import base64
import json
from django.http import HttpResponse
from django.shortcuts import render
import os
from natsort import natsorted
import numpy as np
import dtw
from .utilities import RecordingRhythm, RecordingMelody, plot_recordings, scoring


# Create your views here.
def index(request):
    return render(request, 'main/index.html')


def rhtyhm(request, exerciseNumber=None):
    exerciseNumber -= 1  # For list operation

    # Removing old recording
    per_dir_path = r"main/static/home/media/performanslar"
    for fname in os.listdir(per_dir_path):
        os.remove(os.path.join(per_dir_path, fname))

    # Page background choosing
    img_dir_path = r"main/static/home/assets/img"
    img_path_list = ["home/assets/img/" + name for name in os.listdir(img_dir_path) if "rnd" in name]
    random_img_path = img_path_list[np.random.randint(len(img_path_list))]

    # Reading and sorting reference recording
    refs_path = r"main/static/home/media/ritim_sorulari"
    refs_path_list = [os.path.join(r"home/media/ritim_sorulari/", name) for name in
                      natsorted(os.listdir(refs_path))]

    return render(request, 'main/rhythm.html',
                  {
                      'exerciseNumber': exerciseNumber + 1,
                      'random_img_path': random_img_path,
                      'ref_name': [name[:-4] + '_per' for name in natsorted(os.listdir(refs_path))][exerciseNumber],
                      'ref_path': refs_path_list[exerciseNumber]})


def melody(request, exerciseNumber=None):
    exerciseNumber -= 1  # For list operation

    # Removing old recording
    per_dir_path = r"main/static/home/media/performanslar"
    for fname in os.listdir(per_dir_path):
        os.remove(os.path.join(per_dir_path, fname))

    # Page background choosing
    img_dir_path = r"main/static/home/assets/img"
    img_path_list = ["home/assets/img/" + name for name in os.listdir(img_dir_path) if "rnd" in name]
    random_img_path = img_path_list[np.random.randint(len(img_path_list))]

    # Reading and sorting reference recording
    refs_path = r"main/static/home/media/ezgi_sorulari"
    refs_path_list = [os.path.join(r"home/media/ezgi_sorulari/", name) for name in
                      natsorted(os.listdir(refs_path)) if 'transpoze' not in name]
    refs_t_path_list = [os.path.join(r"home/media/ezgi_sorulari/", name) for name in
                        natsorted(os.listdir(refs_path)) if 'transpoze' in name]

    return render(request, 'main/melody.html',
                  {
                      'exerciseNumber': exerciseNumber + 1,
                      'random_img_path': random_img_path,
                      'ref_name': [name[:-4] + '_per' for name in natsorted(os.listdir(refs_path))][exerciseNumber],
                      'ref_path': refs_path_list[exerciseNumber],
                      'ref_path_transpose': refs_t_path_list[exerciseNumber]})


def grade_rhythm(request, exerciseNumber):
    img_dir_path = r"main/static/home/assets/img"
    img_path_list = ["home/assets/img/" + name for name in os.listdir(img_dir_path) if "rnd" in name]
    random_img_path = img_path_list[np.random.randint(len(img_path_list))]

    ref_dir_path = r"main/static/home/media/ritim_sorulari"
    per_dir_path = r"main/static/home/media/performanslar"
    ref_path = ref_dir_path + "/r" + str(exerciseNumber) + ".wav"
    per_path = per_dir_path + "/r" + str(exerciseNumber) + "_per.wav"

    ref = RecordingRhythm(ref_path)
    per = RecordingRhythm(per_path)

    ref_data = ref.ODF_crop_fixLen['hfc']
    per_data = per.ODF_crop_fixLen['hfc']
    # print("\n\n","="*30)
    # print(per_data)
    graph_div = plot_recordings(ref_data, per_data)

    # =============================================Scoring=============================================

    per_grade, message = scoring(ref_data, per_data)

    if exerciseNumber < 10:
        next_exercise_link = f"../../{exerciseNumber + 1}"
        next_exercise_message = "Sonraki Soru"
    else:
        next_exercise_link = f"../../1"
        next_exercise_message = "Başa Dön"

    return render(request, 'main/grading.html', {
        'grade': per_grade,
        'message': message,
        'exerciseNumber': exerciseNumber,
        'next_exercise_link': next_exercise_link,
        'next_exercise_message': next_exercise_message,
        'random_img_path': random_img_path,
        'ref_path': "home/media/ritim_sorulari/" + ref_path.split('/')[-1],
        'per_path': "home/media/performanslar/" + per_path.split('/')[-1],
        'graph_div': graph_div
    })


def grade_melody(request, exerciseNumber):
    img_dir_path = r"main/static/home/assets/img"
    img_path_list = ["home/assets/img/" + name for name in os.listdir(img_dir_path) if "rnd" in name]
    random_img_path = img_path_list[np.random.randint(len(img_path_list))]

    ref_dir_path = r"main/static/home/media/ezgi_sorulari"
    per_dir_path = r"main/static/home/media/performanslar"
    ref_path = ref_dir_path + "/e" + str(exerciseNumber) + ".wav"
    per_path = per_dir_path + "/e" + str(exerciseNumber) + "_per.wav"

    ref_recording = RecordingMelody(ref_path)
    std_recording = RecordingMelody(per_path)
    # Perform dtw alignment
    alignment = dtw.dtw(ref_recording.f0_series_cent, std_recording.f0_series_cent)
    ref_data = ref_recording.f0_series_cent[alignment.index1]
    per_data = std_recording.f0_series_cent[alignment.index2]

    graph_div = plot_recordings(ref_data, per_data)

    per_grade, message = scoring(ref_data, per_data, alignment, is_melody=True)

    if exerciseNumber < 10:
        next_exercise_link = f"../../{exerciseNumber + 1}"
        next_exercise_message = "Sonraki Soru"
    else:
        next_exercise_link = f"../../1"
        next_exercise_message = "Başa Dön"

    return render(request, 'main/grading.html', {
        'grade': per_grade,
        'message': message,
        'exerciseNumber': exerciseNumber,
        'next_exercise_link': next_exercise_link,
        'next_exercise_message': next_exercise_message,
        'random_img_path': random_img_path,
        'ref_path': "home/media/ezgi_sorulari/" + ref_path.split('/')[-1],
        'per_path': "home/media/performanslar/" + per_path.split('/')[-1],
        'graph_div': graph_div
    })


def save_audio(request):
    data_str = request.body.decode('ascii')
    data = json.loads(data_str)
    audio = data['audio']
    title = data.get('title')
    wav_file = open(f"{title}.wav", "wb")
    decode_string = base64.b64decode(audio)
    wav_file.write(decode_string)
    return HttpResponse('Saved')
