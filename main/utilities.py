import joblib
import numpy as np

from essentia.standard import OnsetDetection, Windowing, FFT, CartesianToPolar, Onsets, FrameGenerator
from essentia import Pool, array
import resampy
from librosa import load, pyin, note_to_hz

from scipy.signal import medfilt
from scipy import spatial, stats

import plotly
from plotly.subplots import make_subplots
import plotly.graph_objects as go

import time


class RecordingRhythm(object):

    def __init__(self, file_path):

        self.file_path = file_path.split('/')[-1]

        # Read audio file
        audio_sig, _ = load(file_path)  # wav dosyasını okuma
        # Amplitude normalization
        audio_sig = audio_sig / np.max(np.abs(audio_sig))  # normalizasyon
        self._extract_onsets(audio_sig)  # vuruşları tespit etme

    def _extract_onsets(self, audio_sig):
        """
        Extract the onset vectors from the waveform

        Parameters
        ----------
        audio_sig: np.array
           The waveform of the audio record

        Returns
        -------
        onsets : np.array
            Onsets vector for the waveform

        Slightly modified version of:
        https://github.com/MTG/essentia/blob/master/src/examples/tutorial/example_onsetdetection.py
        """

        WINDOW_SIZE = 1024
        WINDOWING_METHOD = 'hann'

        # Computing onset detection functions
        self.ODF = {}  # Onset detection function; method-> array
        self.ODF_crop = {}  # Onset detection function cropped from first onset to last; method-> array
        self.ODF_crop_fixLen = {}  # Onset detection function cropped and resampled to ODF_LEN
        self.onsets = {}  # Onsets; method -> array

        w = Windowing(type=WINDOWING_METHOD)
        fft = FFT()
        c2p = CartesianToPolar()
        onsets = Onsets()

        # for method in ['hfc','flux','complex']: # Disabled due to memory concerns
        for method in ['hfc']:
            od = OnsetDetection(method=method)
            pool = Pool()
            for frame in FrameGenerator(audio_sig, frameSize=WINDOW_SIZE):
                mag, phase, = c2p(fft(w(frame)))
                pool.add('features', od(mag, phase))

            self.ODF[method] = pool['features']
            self.onsets[method] = onsets(array([pool['features']]), [1])
            # removing onsets with low energy
            self._filter_onsets(method)
            # creating cropped versions of ODFs and onsets
            self._crop_ODFs_onsets(method)
            # obtain fixed length ODFs from cropped ODFs
            self._post_filter_cropped_ODF(method)

    def _filter_onsets(self, method):
        """Filtering out onsets with small ODF values"""
        SAMPLE_RATE = 44100
        HOP_SIZE = 512
        THRESH_ONSET_RATIO = 1  # threshold with respect to mean of ODF

        onset_indexes_in_ODF = self.onsets[method] * SAMPLE_RATE / HOP_SIZE
        ODF_threshold = np.mean(self.ODF[method]) * THRESH_ONSET_RATIO

        onsets_to_keep = []
        for i in range(onset_indexes_in_ODF.size):
            # if mean of new two samples of ODF is lower than the threshold, remove onset
            ind = int(onset_indexes_in_ODF[i])
            if 2 < ind < (self.ODF[method].size - 3) and np.mean(
                    self.ODF[method][ind + 1:ind + 3]) > ODF_threshold:
                onsets_to_keep.append(True)
            else:
                onsets_to_keep.append(False)

        self.onsets[method] = self.onsets[method][onsets_to_keep]

    def _crop_ODFs_onsets(self, method):
        SAMPLE_RATE = 44100
        HOP_SIZE = 512

        onset_indexes_in_ODF = self.onsets[method] * SAMPLE_RATE / HOP_SIZE
        if len(onset_indexes_in_ODF):
            first_onset_ind = int(onset_indexes_in_ODF[0])
            last_onset_ind = int(onset_indexes_in_ODF[-1])
            self.ODF_crop[method] = self.ODF[method][first_onset_ind:last_onset_ind]

        else:
            self.ODF_crop[method] = np.zeros((10,))
            self.ODF_crop[method][0] = 1

    def _post_filter_cropped_ODF(self, method):
        ODF_LEN = 128
        self.ODF_crop_fixLen[method] = resampy.resample(self.ODF_crop[method], self.ODF_crop[method].size, ODF_LEN,
                                                        axis=-1)
        # amplitude normalize
        self.ODF_crop_fixLen[method] = self.ODF_crop_fixLen[method] / np.max(self.ODF_crop_fixLen[method])

        # Resample may return one element less, padd zero in that case
        if self.ODF_crop_fixLen[method].size < ODF_LEN:
            self.ODF_crop_fixLen[method] = np.concatenate(
                (self.ODF_crop_fixLen[method], np.zeros((ODF_LEN - self.ODF_crop_fixLen[method].size,))))


class RecordingMelody(object):

    def __init__(self, file_path):
        CENT_REF55HZ = 55.0
        MINIMUM_FREQUENCY = 50
        MED_FILT_KERNEL_SIZE_F0 = 5
        audio_sig, fs = load(file_path)
        start_f0 = time.time()
        self.f0_series_hz, voiced_flag, voiced_probs = pyin(audio_sig, fmin=note_to_hz('C2'),
                                                            fmax=note_to_hz('C7'))
        print("f0_series_hz suresi:", time.time() - start_f0)
        # self.freqHopLen_sec = time_data[1] - time_data[0]
        # self.f0_series_hz[confidence < CREPE_F0_CONFIDENCE_LIMIT] = 0
        self.f0_series_hz = medfilt(self.f0_series_hz, kernel_size=MED_FILT_KERNEL_SIZE_F0)
        # Drop all low frequency values from the series
        self.f0_series_hz = self.f0_series_hz[self.f0_series_hz > MINIMUM_FREQUENCY]

        tonic = CENT_REF55HZ
        threshold = MINIMUM_FREQUENCY
        invalid_val = 0
        self.f0_series_cent = hz2cents_array(self.f0_series_hz, tonic, threshold, invalid_val)


def hz2cents_array(f0_series_hz, tonic, threshold, invalid_val):
    # TODO: documentation on this method
    """
    :param f0_series_hz:
    :param tonic:
    :param threshold: float
        Lower frequency threshold for voicing decision
    :param invalid_val: float
        The value assigned to invalid samples
    :return:
    """
    CENTS_PER_OCTAVE = 1200.0

    ind_valid = np.where(f0_series_hz > threshold)[0]  # indexes of valid values
    output = invalid_val * np.ones(f0_series_hz.size)
    output[ind_valid] = CENTS_PER_OCTAVE * np.log2(f0_series_hz[ind_valid] / float(tonic))
    return output


def compute_features(ref_data, per_data, alignment):
    # Features computed from dtw alignment

    diff_hist_bins = [0, 25, 50, 75, 100, 125, 150, 200, 1200]

    features = {'dtw_norm_dist': [alignment.normalizedDistance],
                'dtw_len_mod_ref': [alignment.index1.size / ref_data.size],
                'dtw_len_mod_std': [alignment.index2.size / per_data.size]}
    # Features computed from diff signal
    diff_series = np.abs(per_data - ref_data)

    features['mean_diff'] = [np.mean(diff_series)]
    features['std_diff'] = [np.std(diff_series)]
    features['last_10perc_mean_diff'] = [np.mean(diff_series[-diff_series.size // 10:])]

    diff_hist = np.histogram(diff_series, diff_hist_bins)[0]
    diff_hist = diff_hist / np.sum(diff_hist)
    for i, val in enumerate(diff_hist):
        features['diff_hist' + str(i)] = [val]

    return np.array(list(features.values())).reshape(1, 14)


def plot_recordings(ref_data, per_data):
    """Returns a plot as a div for reference and performance data

        Parameters
        ----------
        ref_data : list
            The data of reference
        per_data : list
            The data of performance

        Returns
        -------
        str
            A div that includes 2 rows (reference and performance) of plot
        """
    fig = make_subplots(rows=2, cols=1, vertical_spacing=0.0)

    fig.add_trace(
        go.Scatter(x=np.arange(len(ref_data)), y=ref_data, mode='lines', name='Referans'),
        row=1, col=1
    )
    fig.add_trace(
        go.Scatter(x=np.arange(len(per_data)), y=per_data, mode='lines', name='Performans'),
        row=2, col=1
    )
    fig.update_layout(
        title="Kayıtların Grafiksel Gösterimi",
        legend=dict(
            yanchor="top",
            y=0.99,
            xanchor="right",
            x=0.99
        ),
        paper_bgcolor='rgba(255,255,255,0.7)',
        plot_bgcolor='rgba(255,255,255,0.9)'
    )

    fig.update_layout(yaxis_visible=False, yaxis_showticklabels=False, )
    fig.update_yaxes(visible=False, showticklabels=False, row=2, col=1)

    fig.update_xaxes(showticklabels=False, row=1, col=1)
    fig.update_xaxes(title='Index', row=2, col=1)
    fig.update_xaxes(showticklabels=False, row=1, col=1)

    graph_div = plotly.offline.plot(fig, auto_open=False, config={'staticPlot': True}, output_type="div")
    return graph_div


def prediction_rhythm(ref_data, per_data):
    # Preprocessing
    correlation = spatial.distance.correlation
    cosine = spatial.distance.cosine
    cityblock = spatial.distance.cityblock
    euclidean = spatial.distance.euclidean
    wasserstein = stats.wasserstein_distance

    DISTANCE_FUNCTIONS_ODF_onsets = [correlation, cosine, cityblock, euclidean, wasserstein]

    if ref_data.size != per_data.size:  # TODO: to be corrected, vector size may come out to be 127 or 128
        min_len = min(ref_data.size, per_data.size)
        ref_data = ref_data[:min_len]
        per_data = per_data[:min_len]

    dists_ODFs = [distance_function(ref_data, per_data) for distance_function in
                  DISTANCE_FUNCTIONS_ODF_onsets]

    # Prediction
    model_path = r"main/static/home/models/lightgbm_rhythm.pkl"

    model = joblib.load(model_path)
    grade = model.predict(np.array(dists_ODFs).reshape(1, 5))[0]

    return grade + 1


def prediction_melody(ref_data, per_data, alignment):
    features = compute_features(ref_data, per_data, alignment)

    model_path = r"main/static/home/models/lightgbm_melody.pkl"

    model = joblib.load(model_path)
    grade = model.predict(features.reshape(1, 14))[0]

    return grade + 1


def scoring(ref_data, per_data, alignment=None, is_melody=False):
    """Returns grade prediction and message

            Parameters
            ----------
            ref_data : np.array
                The data of reference
            per_data : np.array
                The data of performance
            alignment:

            is_melody : bool, optional
                A flag used to choose rhythm or melody (default is
                False)

            Returns
            -------
            tuple (int, str)
                Model prediction and a message about grade

            """
    start = time.time()
    if is_melody:
        per_grade = prediction_melody(ref_data, per_data, alignment)
    else:
        per_grade = prediction_rhythm(ref_data, per_data)
    print("Gecen sure:", time.time() - start)
    if per_grade == 4:
        message = "Tebrikler!!! Daha iyisi olamazdı."
    elif per_grade == 3:
        message = "Tebrikler!!! Biraz daha çalışma ile mükemmel olabilirsin."
    elif per_grade == 2:
        message = "Biraz daha çalışman gerekiyor."
    else:
        message = "Kaldın!!! Çok daha fazla çalışman gerekiyor..."
    return per_grade, message
