FROM python:3.9.13-slim-buster

WORKDIR /app

RUN apt-get update &&  apt-get install libsndfile1 ffmpeg -y

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

CMD ["python3","manage.py","runserver", "0.0.0.0:8000"]
